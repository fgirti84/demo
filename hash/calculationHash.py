from sys import argv
from services import *
try:
    FLAG = argv[1]
    PATH_DIR = argv[2]
    LIST_VM = argv[3:]
except Exception as err:
    print('Ошибка считывания параметров ', err)
filelist = []
list_json = None
try:
    if FLAG == 'check':
        list_json = check_json_file(PATH_DIR, LIST_VM)
    for item in LIST_VM:
        hash_list = []
        for root, dirs, files in os.walk(os.path.join(PATH_DIR, item)):
            for file in files:
                filelist.append(os.path.join(root, file))
                calculated_hash = cal_hash(os.path.join(root, file))
                absolute_path = os.path.abspath(os.path.join(root, file))
                hash_list.append({
                    "Hash": calculated_hash,
                    "Path": absolute_path
                })
        if FLAG == 'check' and list_json:
            check_hash_json(item, hash_list, list_json, PATH_DIR)
        elif FLAG != 'check':
            create_hash_json(os.path.abspath(os.path.join(PATH_DIR, item)), hash_list)
    if FLAG == 'check' and list_json:
        send_result()

except Exception as err:
    print('Ошибка в основном коде ', err)
