import hashlib
import json
import os
from sys import argv
import requests
import pathlib


class MyError(Exception):
    def __init__(self, text):
        self.txt = text


try:
    data_mes_bot = ''
    text_g = ''
    path = argv[0].split("calculationHash")[0]
    with open(os.path.abspath(os.path.join(path, 'message_bot.json')), 'r') as f:
        data_mes_bot = json.load(f)
except Exception as err:
    print('Ошибка не найден файл с настройками \n', err)
finally:
    def create_hash_json(item, hash_list):
        try:
            with open(str(item) + "-hash.json", 'w+') as outfile:
                json.dump(hash_list, outfile)
        except Exception as err:
            print('Ошибка создания json \n', err)

"""Расчет хэша"""


def cal_hash(path_to_file):
    """Размер буффера"""
    BUF_SIZE = 65536
    """Метод хэширования"""
    md5 = hashlib.md5()
    try:
        with open(path_to_file, 'rb') as f:
            while True:
                data = f.read(BUF_SIZE)
                if not data:
                    break
                md5.update(data)
    except Exception as err:
        print('Ошибка при расчете hash \n', err)
    return format(md5.hexdigest()).upper()


def check_json_file(path, list_dirs):
    try:
        list_json = []
        for item in os.listdir(path):
            if item.endswith('.json') and item.find('-') > 0 and str(item).partition('-')[0] in list_dirs:
                list_json.append(item)
    except Exception as err:
        print('Ошибка проверки json файлов \n', err)
    return list_json


def check_hash_json(item, hash_list, list_json, path):
    global text_g
    data = None
    text = ''
    try:
        for i in list_json:
            if str(i).find(item) > -1:
                with open(os.path.join(path, i)) as f:
                    data = json.load(f)
        if data:
            data_mes_bot['title'] = data_mes_bot['title'] + "-" + item + ":"
            for i, el in enumerate(data):
                data_path = el['Path'].split("\\")
                name_file_check = data_path[len(data_path) - 1]
                if hash_list[i]['Hash'] != el['Hash']:
                    text += name_file_check + ' - ERROR!!! \n'
                else:
                    text += name_file_check + ' -  OK \n'
        text_g += text
    except Exception as err:
        print('Ошибка при проверке hash \n', err)


def send_result():
    url = data_mes_bot['url'] + data_mes_bot['token'] + '/sendMessage?chat_id=' + data_mes_bot['chat_id']
    text = data_mes_bot['title'] + '\n' + text_g
    params = {
        'text': text,
    }
    print(text)
    r = requests.get(url=url, params=params)
