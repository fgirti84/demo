export abstract class ShowableComponent {
    protected wasVisible = false;

    protected checkVisible(rect: ClientRect, scroll: { X: number, Y: number }) {
        if (rect.left >= scroll.X && rect.left <= scroll.X + window.innerWidth) {
            if (rect.top > scroll.Y && rect.top <= scroll.Y + window.innerHeight) {
                this.wasVisible = true;
                return true;
            }
        }

        return false;
    }
}
