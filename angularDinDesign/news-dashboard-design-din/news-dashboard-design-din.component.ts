import { Component, Input, OnInit } from "@angular/core";

import { UserService } from "../../../../services/user.service";

import { NewsClientService } from "../../../../services/news-client.service";

import { getDeviceType } from "../../../../shared-module/device-detect/selectors";
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";
import {
  BrowserStorageService,
  StorageKey,
} from "../../../../services/browser-storage.service";
import { VisibilityService } from "../../../../services/visibility.service";
import { filter, startWith, switchMap } from "rxjs/operators";
import { PlatformLocation } from "@angular/common";
import analyze from "rgbaster";
import { OptionsService } from "../../../../admin-module/services/options.service";
import { NewsDashboardPageComponent } from "../news-dashboard-page.component";
import { Observable } from "rxjs";
import { INewsDashboardItem } from "src/model/api/i-news-dashboard-item";
import { UtmContainer } from "src/model/umt-container";
import { __values } from "tslib";
import * as internal from "assert";
@Component({
  selector: "rtt-news-dashboard-design-din",
  templateUrl: "./news-dashboard-design-din.component.html",
  styleUrls: ["./news-dashboard-design-din.component.scss"],
})
export class NewsDesignDinComponent
  extends NewsDashboardPageComponent
  implements OnInit
{
  constructor(
    protected route: ActivatedRoute,
    protected userService: UserService,
    protected newsClientService: NewsClientService,
    protected storage: BrowserStorageService,
    protected router: Router,
    protected visibilityService: VisibilityService,
    protected platformLocation: PlatformLocation,
    protected dictionaryService: OptionsService
  ) {
    super(
      route,
      userService,
      newsClientService,
      storage,
      router,
      visibilityService,
      platformLocation,
      dictionaryService
    );
  }
  lenght: number = 0;
  count: number;
  get isMobile() {
    if (
      /Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(
        navigator.userAgent
      )
    )
      return true;
    return false;
  }
  ngOnInit() {
    super.ngOnInit();
    
  }
  getImageDin(item: INewsDashboardItem, position: number) {
    var image = super.getImage(item, position);
    this.newsList.forEach(t => this.count = t.length);
    if(this.lenght  <  this.count)
    {
      this.getAverageRGB("api/assets/" + image).then(val => this.avarageRGBList.push(val));
      this.lenght =this.lenght+1;
    }
    

    return image;
  }

  getClassBox() {
    if (this.isMobile) return `box-mobile`;
    return `box-desk-style-2`;
  }
  getClass(i: number) {
    if (this.isMobile)
      return `col-lg-4  col-md-4 col-sm-12 pl-2 pr-0 p-sm-2 news-dashboard-item-mobile  p-sm-0 p-2 m-md-0 m-2`;
    return this.environment.imgSize.isSmallByIndexStyle2(i + 1)
      ? `col-lg-4 col-md-4 col-sm-12 pl-0 pr-0 news-dashboard-item-desk  p-sm-0 p-2 m-md-0 m-2`
      : "col-lg-8 col-md-8 col-sm-12 col-12 news-dashboard-item-desk-big p-sm-0 p-2 m-md-0 m-2";
  }
  getIsBig(i: number) {
    return !this.environment.imgSize.isSmallByIndexStyle2(i + 1);
  }
  getStyle(i: number) {
    if (!this.isMobile) return this.avarageRGBList[i];
    return "rgb(255,255,255)";
  }

  async getAverageRGB(img) {
    try {
      var result = await analyze("" + img, { scale: 0.6 });
      return result[0].color;
    } catch (error) {
      return "rgb(217,217,217)";
    }
  }
}
