import { Component, OnInit } from "@angular/core";
import { INewsDashboardItem } from "../../../model/api/i-news-dashboard-item";
import { UserService } from "../../../services/user.service";
import { Observable } from "rxjs";
import { NewsClientService } from "../../../services/news-client.service";
import { environment } from "../../../environments/environment";
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";
import {
  BrowserStorageService,
  StorageKey,
} from "../../../services/browser-storage.service";
import { VisibilityService } from "../../../services/visibility.service";
import { RotatableSharedComponent } from "../../../shared-module/teaser-shared/rotatable-shared-component";
import { UtmContainer } from "../../../model/umt-container";
import { PlatformLocation } from "@angular/common";
import { OptionsService } from "../../../admin-module/services/options.service";
import { getDeviceType } from "src/shared-module/device-detect/selectors";
import { filter, startWith, switchMap } from "rxjs/operators";

@Component({
  selector: "rtt-news-dashboard-page",
  templateUrl: "./news-dashboard-page.component.html",
  styleUrls: ["./news-dashboard-page.component.scss"],
})
export class NewsDashboardPageComponent
  extends RotatableSharedComponent<INewsDashboardItem>
  implements OnInit
{
  environment = environment;
  design: string;
  newsList: Observable<INewsDashboardItem[]>;
  protected locationAsync: Observable<{ iso: string; name: string }>;
  protected avarageRGBList = [];
  protected device: string;
  protected location: { iso: string; name: string };

  protected utmContainer: UtmContainer;

  constructor(
    protected route: ActivatedRoute,
    protected userService: UserService,
    protected newsClientService: NewsClientService,
    protected storage: BrowserStorageService,
    protected router: Router,
    protected visibilityService: VisibilityService,
    protected platformLocation: PlatformLocation,
    protected dictionaryService: OptionsService
  ) {
    super(20);
  }
  
  getUtmContainer(){
    return this.utmContainer;
  }
  getDevice(){
    return this.device;
  }
  getLocationAsync(){
    return this.locationAsync;
  }
  getNewsList(){
    return this.newsList;
  }
 
  ngOnInit() {
    this.dictionaryService.getOptions()
    .forEach(p => p.options
    .forEach(el => {
      if(el.key === "DESIGN_STYLE") 
      this.design = el.value;
      } 
      ));
      const paramMap = this.route.snapshot.queryParamMap;
    this.utmContainer = {
      utmSource: paramMap.get("utm_source"),
      utmMedium: paramMap.get("utm_medium"),
      utmCampaign: paramMap.get("utm_campaign"),
      utmContent: paramMap.get("utm_content"),
      utmTerm: paramMap.get("utm_term"),
    };
   
    this.device = getDeviceType();

    this.locationAsync = this.router.events.pipe(
      filter((f) => f instanceof NavigationEnd),
      startWith(null),
      switchMap((r) => this.userService.getLocation())
    );

    this.newsList = this.baseOnInit(
      this.newsClientService.viewedNews,
      this.locationAsync,
      (location, m) => {
        this.location = location;
        return this.newsClientService.get(
          location.iso,
          this.route.firstChild != null
            ? this.route.firstChild.snapshot.params["slug"]
            : null,
          m
        );
      }
    );

    this.router.events.subscribe((event) => {
      this.setNeedConcat(false);
    });

    this.platformLocation.onPopState(() => {
      history.pushState(null, "", "/");
    });
    
  }

  handleNewsClick(id, $event) {
    if ($event && $event.which !== 2) {
      return;
    } else if (!$event) {
      this.storage.setKey(StorageKey.CLICKED_NEWS_ITEM, {
        id,
        time: new Date(),
      });
    }
    this.newsClientService
      .click(id, this.location.iso, this.device, this.utmContainer)
      .subscribe();
  }

  getImage(item: INewsDashboardItem, position: number) {
    return environment.imgSize.isSmallByIndex(position)
      ? item.smallImagePath
      : item.bigImagePath
      ? item.bigImagePath
      : item.smallImagePath;
  }

  handleShown(id: number) {
    this.visibilityService.showNews({
      newsId: id,
      utmContainer: this.utmContainer,
    });
  }

  handleScroll() {
    this.reset();
  }
}
