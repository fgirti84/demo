import { Component, Input, OnInit } from "@angular/core";
import { UserService } from "../../../../services/user.service";
import { NewsClientService } from "../../../../services/news-client.service";
import { getDeviceType } from "../../../../shared-module/device-detect/selectors";
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";
import {
  BrowserStorageService,
  StorageKey,
} from "../../../../services/browser-storage.service";
import { VisibilityService } from "../../../../services/visibility.service";
import { filter, startWith, switchMap } from "rxjs/operators";
import { PlatformLocation } from "@angular/common";
import { OptionsService } from "../../../../admin-module/services/options.service";
import {NewsDashboardPageComponent} from "../news-dashboard-page.component";
import { INewsDashboardItem } from "src/model/api/i-news-dashboard-item";
import { UtmContainer } from "src/model/umt-container";
import { Observable } from "rxjs";
@Component({
  selector: "rtt-news-dashboard-design-original",
  templateUrl: "./news-dashboard-design-original.component.html",
  styleUrls: ["./news-dashboard-design-original.component.scss"],
})
export class NewsDesignOriginalComponent
  extends NewsDashboardPageComponent
  implements OnInit
{
  constructor(
    protected route: ActivatedRoute,
    protected userService: UserService,
    protected newsClientService: NewsClientService,
    protected storage: BrowserStorageService,
    protected router: Router,
    protected visibilityService: VisibilityService,
    protected platformLocation: PlatformLocation,
    protected dictionaryService: OptionsService
  ) {
    super(route,userService,newsClientService,storage,router,visibilityService,platformLocation,dictionaryService);
  }
  ngOnInit() {
    super.ngOnInit();
  }
  getClass(i: number) {
    return this.environment.imgSize.isSmallByIndex(i) ?
      `col-lg-3 col-md-4 col-sm-5 col-12 news-dashboard-item p-sm-0 p-2 m-md-0 m-2` :
      "col-lg-6 col-md-8 col-sm-10 col-12 news-dashboard-item p-sm-0 p-2 m-md-0 m-2";
  }
}
