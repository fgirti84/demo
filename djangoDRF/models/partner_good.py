import uuid
from django.contrib.auth import get_user_model
from django.db import models
from django.template.defaultfilters import safe
from django.utils.translation import gettext_lazy as _

from catalog.models import (
    Partner,
    PartnerStock,
    model_image_path
)

User = get_user_model()


class PartnerGoodDeliveryMethod(models.Model):
    name = models.CharField(
        _("Method name"),
        max_length=250,
    )
    description = models.TextField(
        blank=True,
        null=True,
    )
    is_active = models.BooleanField(
        _("Active method"),
        default=False,
        blank=True,
    )

    class Meta:
        verbose_name = _("Delivery method")
        verbose_name_plural = _("Delivery methods")


class PartnerGoodCategory(models.Model):
    parent = models.ForeignKey(
        "self",
        null=True,
        on_delete=models.CASCADE,
        related_name='subcategories'
    )
    name = models.CharField(
        _("Category name"),
        max_length=250,
    )
    text = models.TextField(
        blank=True,
        null=True,
    )
    image = models.ImageField(
        null=True,
        blank=True,
        verbose_name=_("Image"),
        help_text=_("Image size at least 100x100")
    )

    class Meta:
        verbose_name = _("PartnerGoodCategory")
        verbose_name_plural = _("PartnerGoodCategories")


class PartnerGood(models.Model):
    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False
    )
    name = models.CharField(
        _("Product name"),
        max_length=250,
    )
    description = models.TextField(
        _("Product description"),
    )
    price = models.PositiveIntegerField(
        _("Price"),
    )

    discount_price = models.PositiveIntegerField(
        _("Discount price"),
        null=True,
        blank=True,
    )
    url = models.URLField(
        "URL",
        null=True,
        blank=True
    )
    partner = models.ForeignKey(
        Partner,
        verbose_name=_("Partner"),
        on_delete=models.CASCADE,
    )
    delivery_method = models.ForeignKey(
        PartnerGoodDeliveryMethod,
        verbose_name=_("Delivery method"),
        on_delete=models.CASCADE,
        null=True
    )
    on_main = models.BooleanField(
        _("Display on the main page, in the popular section."),
        default=False,
        blank=True,
    )
    show_in_went_list = models.BooleanField(
        _("Show in the 'Want' list"),
        default=False,
        blank=True
    )
    categories = models.ManyToManyField(
        PartnerGoodCategory,
        verbose_name=_("Categories"),
        related_name='partner_good'
    )
    weight = models.DecimalField(
        _("Weight"),
        max_digits=8,
        decimal_places=3
    )
    length = models.PositiveIntegerField(
        _("Length"),
    )
    width = models.PositiveIntegerField(
        _("Width"),
    )
    height = models.PositiveIntegerField(
        _("Height"),
    )
    stocks = models.ManyToManyField(
        PartnerStock,
        verbose_name=_("PartnerStock")
    )
    dt_create = models.DateTimeField(
        _('Date create'),
        auto_now_add=True
    )
    create_user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='partner_good_create_user',
        null=True,
        blank=True,
    )
    dt_update = models.DateTimeField(
        _("Time of update"),
        null=True,
        blank=True,
    )
    update_user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='partner_good_update_user',
        null=True,
        blank=True,
    )
    is_active = models.BooleanField(
        _("Active product"),
        default=False,
        blank=True,
    )


    def __str__(self):
        return f'id: {self.id} name: {self.name}'

    class Meta:
        verbose_name = _("PartnerGood")
        verbose_name_plural = _("PartnerGood")


class PartnerGoodImage(models.Model):
    image = models.ImageField(
        null=True,
        help_text=_("Image size at least 600x600"),
        verbose_name=_("partner good image")
    )

    product = models.ForeignKey(
        PartnerGood,
        verbose_name=_("PartnerGood"),
        on_delete=models.CASCADE,
        related_name="partner_good",
        null=True,
        blank=True,
    )
    is_main_image = models.BooleanField(
        _("Main image"),
        default=False,
        blank=True,
        null=True
    )

    def __str__(self):
        return f'{self.image}'

    class Meta:
        verbose_name = _("PartnerGoodImage")
        verbose_name_plural = _("PartnerGoodImages")


class PartnerGoodProperty(models.Model):
    name = models.CharField(
        _("Property name"),
        max_length=250,
        unique=True,
        db_index=True,
    )

    class Meta:
        verbose_name = _("PartnerGood property")
        verbose_name_plural = _("PartnerGood properties")


class PartnerGoodPropertyThrough(models.Model):
    product = models.ForeignKey(
        PartnerGood,
        related_name='product_properties',
        on_delete=models.CASCADE,
        verbose_name=_("PartnerGood"),
    )
    product_property = models.ForeignKey(
        PartnerGoodProperty,
        verbose_name=_("PartnerGood property"),
        on_delete=models.CASCADE,
    )
    value = models.TextField(
        _("Property value"),
        max_length=250,
    )

    def __str__(self):
        return self.product_property.name
