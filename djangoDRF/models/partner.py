from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import gettext_lazy as _

from catalog.models import PartnerRegistrationRequest

User = get_user_model()


class Partner(models.Model):
    id = models.BigAutoField(
        primary_key=True,
        editable=False
    )
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='partner_user',
    )
    registration_request = models.ForeignKey(
        PartnerRegistrationRequest,
        on_delete=models.CASCADE,
        related_name='partner_registration_request',
    )
    name = models.CharField(
        _("Partner name"),
        max_length=500,
    )
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Partner")
        verbose_name_plural = _("Partners")
