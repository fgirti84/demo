from django.db import models
from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _

from catalog.models import Partner

User = get_user_model()


class PartnerAssistantStatusEnum(models.TextChoices):
    posted = ('posted', _('Posted'))
    approved = ('approved', _('Approved'))
    rejected = ('rejected', _('Rejected'))


class PartnerAssistant(models.Model):
    partner = models.ForeignKey(
        Partner,
        on_delete=models.CASCADE,
        related_name='partner',
    )
    phone = models.CharField(
        _('Phone assistant'),
        max_length=15
    )
    dt_create = models.DateTimeField(
        _('Date create'),
        auto_now_add=True
    )
    status = models.CharField(
        _("Status"),
        choices=PartnerAssistantStatusEnum.choices,
        default=PartnerAssistantStatusEnum.posted,
        max_length=15,
    )
    dt_accept = models.DateTimeField(
        _('Date accept'),
        null=True,
        blank=True,
        default=None
    )
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='partner_assistant_user',
        null=True,
        blank=True,
    )
    is_active = models.BooleanField(default=True)

    class Meta:
        verbose_name = _("PartnerAssistant")
        verbose_name_plural = _("PartnerAssistants")
