from django.contrib.auth import get_user_model
from django.contrib.gis.db.models import PointField
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from catalog.models import Partner

User = get_user_model()


class PartnerStock(models.Model):
    id = models.BigAutoField(
        primary_key=True,
        editable=False
    )
    partner = models.ForeignKey(
        Partner,
        on_delete=models.CASCADE,
        related_name='partner_stock',
    )
    name = models.CharField(
        _("Stock name"),
        max_length=500,
    )
    city = models.CharField(
        _("Stock city"),
        max_length=255,
    )
    address = models.CharField(
        _("Stock address"),
        max_length=500,
    )
    location_coordinates = models.CharField(
        _("Stock location coordinates"),
        max_length=63,
        blank=True,
        null=True,
    )
    location_point = PointField(
        _("Stock location point"),
        null=True,
        blank=True,
    )
    dt_create = models.DateTimeField(
        _("Time of creation"),
        auto_now_add=True,
    )
    is_active = models.BooleanField(default=True)
    create_user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='partner_stock_create_user',
        null=True,
        blank=True,
    )
    dt_update = models.DateTimeField(
        _("Time of update"),
        null=True,
        blank=True,
    )
    update_user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='partner_stock_update_user',
        null=True,
        blank=True,
    )

    def save(self, *args, **kwargs):
        if self.pk:
            self.dt_update = timezone.now()

        super().save(*args, **kwargs)

    class Meta:
        verbose_name = _("Partner stock")
        verbose_name_plural = _("Partner stocks")
        ordering = ['-dt_create']
