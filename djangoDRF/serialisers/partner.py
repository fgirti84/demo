from rest_framework import serializers as rest_serializers

from catalog import models, serialisers
from users.serializers import V2UserShortSerializer


class PartnerSerializer(rest_serializers.ModelSerializer):
    user = V2UserShortSerializer()
    registration_request = serialisers.PartnerRegistrationRequestShortSerializer()

    class Meta:
        model = models.Partner
        fields = '__all__'
