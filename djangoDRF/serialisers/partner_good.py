from django.core.validators import FileExtensionValidator
from django.utils import timezone
from rest_framework import serializers
import typing as t
from catalog import models, serialisers
from rest_framework import exceptions
from catalog.services import partner_good_service


class PartnerGoodDeliveryMethodSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.PartnerGoodDeliveryMethod
        fields = '__all__'

class PartnerGoodDeliveryMethodIsActiveSerializer(serializers.ModelSerializer):
    is_active = serializers.BooleanField(
        read_only=True
    )

    class Meta:
        model = models.PartnerGoodDeliveryMethod
        fields = [
            'is_active'
        ]

    def update(self, instance, validated_data):
        validated_data['is_active'] = not instance.is_active
        up_instance = super().update(instance, validated_data)
        return up_instance

class PartnerGoodImageSerializer(serializers.ModelSerializer):
    image = serializers.ImageField(validators=[
        FileExtensionValidator(
            allowed_extensions=[
                'png',
                'jpeg',
                'jpg'
            ]
        )
    ]
    )

    class Meta:
        model = models.PartnerGoodImage
        exclude = [
            'product'
        ]

    def validate(self, attrs) -> t.Dict:
        data = super().validate(attrs)
        product = models.PartnerGood.objects.filter(id=self.context['view'].kwargs['good_id']).first()
        if product is None:
            raise exceptions.ValidationError(detail={'Товар не найден'})
        data['product'] = product
        partner_good_service.check_image(data['image'], product)
        return data

    def create(self, validated_data):
        is_main = validated_data['is_main_image']
        validated_data['is_main_image'] = partner_good_service.check_main_image(validated_data['product'], is_main)
        instance = super().create(validated_data)
        return instance


class PartnerGoodPropertySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.PartnerGoodProperty
        fields = '__all__'

    def create(self, validated_data):
        partner_good_service.check_duplicate(models.PartnerGoodProperty, validated_data)
        super().create(validated_data)
        return validated_data


class PartnerGoodPropertyThroughSerializer(serializers.Serializer):
    # Разобраться с id. Если переименовать поле id, не работает
    id = serializers.IntegerField()
    value = serializers.CharField(max_length=255)

    class Meta:
        fields = [
            'id',
            'value',
        ]


class PartnerGoodCategoryGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.PartnerGoodCategory
        fields = '__all__'


class PartnerGoodCategorySerializer(serializers.ModelSerializer):
    image = serializers.ImageField(
        default=None,
        validators=[
            FileExtensionValidator(allowed_extensions=[
                'png',
                'jpeg',
                'jpg'
            ]
            )
        ]
    )
    subcategories = PartnerGoodCategoryGetSerializer(
        many=True,
        read_only=True
    )

    class Meta:
        model = models.PartnerGoodCategory
        fields = '__all__'

    @property
    def validated_data(self):
        data = super().validated_data
        if data['image']:
            partner_good_service.check_size_image(data['image'])
        return data

    def create(self, validated_data):
        partner_good_service.check_duplicate(models.PartnerGoodCategory, validated_data)
        super().create(validated_data)
        return validated_data


class PartnerGoodIsActiveSerializer(serializers.ModelSerializer):
    is_active = serializers.BooleanField(
        read_only=True
    )

    class Meta:
        model = models.PartnerGood
        fields = [
            'is_active'
        ]

    def update(self, instance, validated_data):
        if not instance.is_active:
            images = models.PartnerGoodImage.objects.filter(product=instance)
            if not images:
                raise exceptions.ValidationError(
                    detail={'Невозможно активировать товар, так как не загружено главное изображение'})
        validated_data['is_active'] = not instance.is_active
        up_instance = super().update(instance, validated_data)
        return up_instance


class PartnerGoodSerializer(serializers.ModelSerializer):
    product_properties = PartnerGoodPropertyThroughSerializer(many=True)
    images = serializers.SerializerMethodField()
    main_image = serializers.SerializerMethodField()
    is_active = serializers.BooleanField(read_only=True)
    weight = serializers.DecimalField(
        max_digits=8,
        decimal_places=3,
        max_value=10000,
        min_value=0.001
    )
    length = serializers.IntegerField(
        max_value=1500,
        min_value=0
    )
    width = serializers.IntegerField(
        max_value=1500,
        min_value=0
    )
    height = serializers.IntegerField(
        max_value=1500,
        min_value=0
    )

    class Meta:
        model = models.PartnerGood
        exclude = [
            'partner',
            'dt_create',
            'create_user',
            'dt_update',
            'update_user'
        ]

    @property
    def validated_data(self):
        data = super().validated_data
        data['create_user_id'] = self.context['request'].user.id
        if self.context['request'].method in ['PUT', 'PATCH']:
            data['update_user_id'] = self.context['request'].user.id
            data['dt_update'] = timezone.now()
        return data

    def validate(self, attrs) -> t.Dict:
        data = super().validate(attrs)
        data['partner'] = models.Partner.objects.filter(id=self.context['view'].kwargs['partner_id']).first()
        if data['partner']:
            return data
        else:
            raise exceptions.ValidationError(detail=data)

    def create(self, validated_data):
        product_properties = validated_data.pop('product_properties')
        categories = validated_data.pop('categories')
        stocks = validated_data.pop('stocks')
        instance = super().create(validated_data)
        instance.categories.set(categories)
        instance.stocks.set(stocks)
        partner_good_service.create_prop(product_properties, instance.id)
        return instance

    def update(self, instance, validated_data):
        if 'product_properties' in validated_data:
            validated_data = partner_good_service.update_prop(validated_data, instance.id)
        instance_new = super().update(instance, validated_data)
        return instance_new

    def get_images(self, obj):
        request = self.context.get('request')
        images = models.PartnerGoodImage.objects.filter(product=obj)
        serializer = PartnerGoodImageSerializer(
            list(images),
            many=True,
            context={"request": request}
        )
        return serializer.data

    def get_main_image(self, obj):
        images = self.get_images(obj)
        for main_image in images:
            if main_image['is_main_image']:
                return main_image
        return {}


class PartnerGoodGetSerializer(PartnerGoodSerializer):
    categories = PartnerGoodCategoryGetSerializer(many=True)
    stocks = serialisers.PartnerStockSerializer(many=True)
    delivery_method = PartnerGoodDeliveryMethodSerializer()

    class Meta:
        model = models.PartnerGood
        fields = '__all__'
