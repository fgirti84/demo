import typing as t

from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from rest_framework import exceptions
from rest_framework import serializers

from catalog.models import PartnerAssistant


class PartnerAssistantSerializer(serializers.ModelSerializer):
    class Meta:
        model = PartnerAssistant
        fields = '__all__'

    @property
    def validated_data(self):
        data = super().validated_data
        return data

    def validate(self, attrs) -> t.Dict:
        data = super().validate(attrs)
        instance = self.instance
        if instance.status != 'posted':
            raise exceptions.ValidationError(detail=data)
        return data


class PartnerAssistantCreateSerializer(serializers.ModelSerializer):
    user_id = serializers.IntegerField(allow_null=True, required=False)

    class Meta:
        model = PartnerAssistant
        fields = ['phone', 'user_id']


class PartnerAssistantAcceptedSerializer(serializers.ModelSerializer):
    class Meta:
        model = PartnerAssistant
        fields = ['status']

    @property
    def validated_data(self):
        data = super().validated_data
        data['user_id'] = self.context['request'].user.id
        return data

    def validate(self, attrs) -> t.Dict:
        data = super().validate(attrs)

        if data['status'] not in ['approved', 'rejected']:
            raise exceptions.ValidationError({
                "status": "Статус утверждения указан неверно",
            })

        if self.instance.status != 'posted':
            raise exceptions.NotAcceptable(
                detail=_('The invitation to the assistants of the partner has already been processed'),
                code='partner_assistant_invitation_already_processed',
            )

        return data

    def update(self, instance, validated_data):
        instance.user_id = validated_data['user_id']
        instance.status = validated_data['status']
        instance.dt_accept = timezone.now()
        instance.save()
        return instance
