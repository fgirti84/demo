from rest_framework import serializers as rest_serializers

from catalog import models, serialisers


class PartnerStockSerializer(rest_serializers.ModelSerializer):
    partner = serialisers.PartnerSerializer()

    class Meta:
        model = models.PartnerStock
        fields = '__all__'


class PartnerStockCreateSerializer(rest_serializers.ModelSerializer):
    class Meta:
        model = models.PartnerStock
        fields = [
            'name',
            'city',
            'address',
            'location_coordinates',
        ]

    @property
    def validated_data(self):
        data = super().validated_data
        data['partner_id'] = self.context['view'].kwargs['partner_id']
        data['create_user_id'] = self.context['request'].user.id

        if self.context['request'].method in ['PUT', 'PATCH']:
            data['update_user_id'] = self.context['request'].user.id

        return data