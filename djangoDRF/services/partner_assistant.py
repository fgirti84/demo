from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework import exceptions

from catalog import models, serialisers
from helpers.logs import logger

User = get_user_model()


class PartnerAssistantServices:

    def create_assistant(self, partner_id, data, user: User):
        logger.info(f"data: {repr(data)}")
        logger.info(f"user: {repr(user)}")
        try:
            desired_user = User.objects.get(phone=data['phone'])
        except User.DoesNotExist:
            desired_user = None

        if desired_user is None and data.get('user_id') is not None:
            raise exceptions.ValidationError({
                "phone": "Пользователь с указанным номером телефона не обнаружен",
            })

        if desired_user is None and data.get('user_id') is None:
            entry_assistants = self.add_assistant(partner_id, data['phone'])
            return [entry_assistants, status.HTTP_201_CREATED]

        if not desired_user.is_active:
            raise exceptions.ValidationError({
                "phone": "Пользователь с указанным номером телефона заблокирован администратором",
            })

        if desired_user and data.get('user_id') is not None and data['user_id'] != desired_user.id:
            raise exceptions.ValidationError({
                "phone": "Пользователь с указанным номером телефона имеет другой ID",
            })

        double_entry = models.PartnerAssistant.objects.filter(
            partner_id=partner_id,
            phone=data['phone'],
        ).first()

        logger.info(f"desired_user: {repr(desired_user.__dict__ if desired_user else desired_user)}")
        logger.info(f"double_entry: {repr(double_entry.__dict__ if double_entry else double_entry)}")
        logger.info(f"double_entry.status: {repr(double_entry.status if double_entry else None)}")

        if double_entry and double_entry.status == models.PartnerAssistantStatusEnum.posted:
            raise exceptions.ValidationError({
                "phone": "Пользователь с указанным номером телефона уже добавлен в список помощников, "
                         "но еще не подтвердил назначение",
            })

        if double_entry and double_entry.status == models.PartnerAssistantStatusEnum.approved:
            raise exceptions.ValidationError({
                "phone": "Пользователь с указанным номером телефона уже добавлен в список помощников "
                         "и подтвердил назначение",
            })

        if double_entry and double_entry.status == models.PartnerAssistantStatusEnum.rejected:
            return [self.get_data_user(desired_user), status.HTTP_406_NOT_ACCEPTABLE]

        if data.get('user_id') is None:
            return [self.get_data_user(desired_user), status.HTTP_406_NOT_ACCEPTABLE]

        entry_assistants = self.add_assistant(partner_id, data['phone'], desired_user.id)
        return [entry_assistants, status.HTTP_201_CREATED]

    def add_assistant(self, partner_id, phone, user_id=None):
        entry_assistant = models.PartnerAssistant(
            partner_id=partner_id,
            phone=phone,
            user_id=user_id
        )
        entry_assistant.save()
        serialize = serialisers.PartnerAssistantCreateSerializer(entry_assistant)
        return serialize.data

    # Вернуть данные пользователя
    def get_data_user(self, desired_user):
        # Возврат данных пользователя id, first_name, last_name, phone, is_active
        # Если необходимы все поля, просто вернуть desired_user вместо data_desired_user
        data_user = ['id', 'first_name', 'last_name', 'phone', 'is_active']
        data_desired_user = {}
        for key in data_user:
            data_desired_user.update({key: getattr(desired_user, key)})
        return data_desired_user


partner_assistant_service = PartnerAssistantServices()
