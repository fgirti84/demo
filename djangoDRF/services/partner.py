import typing as t

from django.contrib.auth import get_user_model
from django.utils import timezone
from rest_framework import exceptions

from catalog import models

User = get_user_model()


class PartnerService:
    @staticmethod
    def partner_registration_request_resolution(
            user: User,
            instance: models.PartnerRegistrationRequest,
            data: t.Dict
    ):
        if instance.status != models.PartnerRegistrationRequestStatusEnum.posted:
            raise exceptions.NotAcceptable(
                detail="Запрос на регистрацию партнера уже обработан"
            )

        if data.get("resolution") == models.PartnerRegistrationRequestResolutionEnum.apply:
            instance.status = models.PartnerRegistrationRequestStatusEnum.approved
        else:
            instance.status = models.PartnerRegistrationRequestStatusEnum.rejected
            instance.reject_reason = data.get("reject_reason")
            instance.is_read_reject_reason = False

        instance.resolution_user = user
        instance.dt_resolution = timezone.now()
        instance.save()

        # Создаем нового партнера
        if data.get("resolution") == models.PartnerRegistrationRequestResolutionEnum.apply:
            partner = models.Partner(
                registration_request=instance,
                user=instance.user,
                name=instance.name,
                is_active=True,
            )
            partner.save()

        return instance

    @staticmethod
    def partner_registration_request_read_reject_reason(
            user: User,
            instance: models.PartnerRegistrationRequest,
    ):
        if instance.status != models.PartnerRegistrationRequestStatusEnum.rejected:
            raise exceptions.NotAcceptable(
                detail="Статус запроса на регистрацию партнера отличается от 'Отклонена'"
            )

        if instance.user.id != user.id:
            raise exceptions.NotAcceptable(
                detail="Запрос на регистрацию партнера создан другим пользователем"
            )

        if instance.is_read_reject_reason is True:
            raise exceptions.NotAcceptable(
                detail="Ознакомление с причиной отказа уже выполнено"
            )

        instance.is_read_reject_reason = True
        instance.save(update_fields=['is_read_reject_reason'])
        return instance

    @staticmethod
    def partner_is_active_toggle(
            instance: models.Partner,
    ):
        instance.is_active = not instance.is_active
        instance.save(update_fields=['is_active'])
        return instance


partner_service = PartnerService()
