from django.core.files.images import get_image_dimensions
from rest_framework import exceptions
from helpers.logs import logger
import core
from catalog import models, serialisers


class PartnerGoodServices:

    # Проверка дубликатов----------------------------------------------------------------------------------------------#
    def check_duplicate(self, model, val_data):
        list_data = model.objects.all()
        for prop in list_data:
            if str(prop.name).lower() == str(val_data['name']).lower():
                raise exceptions.ValidationError(detail={'Такое свойство уже существует'})

    # Проверка ссылок на объект----------------------------------------------------------------------------------------#
    def check_link_category(self, instance):
        if models.PartnerGoodCategory.objects.filter(parent=instance).exists():
            raise exceptions.ValidationError(
                detail={'type': 'Обнаружены подкатегории, категория не может быть удалена'})
        if models.PartnerGood.objects.filter(categories=instance).exists():
            raise exceptions.ValidationError(
                detail={'type': 'Обнаружены товары, категория не может быть удалена'})

    def check_link_delivery(self, instance):
        if models.PartnerGood.objects.filter(delivery_method=instance).exists():
            raise exceptions.ValidationError(
                detail={'type': 'Обнаружены товары, способ доставки не может быть удален'})

    # Проверка изображения---------------------------------------------------------------------------------------------#
    def check_image(self, image, product):
        self.check_size_image(image)
        self.check_max_images(product)

    def check_size_image(self, image, max_size=5.0, min_width=100, max_width=10000, min_height=100, max_height=10000):
        file_size = image.size
        width, height = get_image_dimensions(image)
        if file_size > max_size * 1024 * 1024:
            raise exceptions.ValidationError("Максимальный размер картинки %sMB" % str(max_size))
        if width < min_width or width > max_width:
            raise exceptions.ValidationError(f"Максимальная {max_width} минимальная {min_width} ширина картинки")
        if height < min_height or height > max_height:
            raise exceptions.ValidationError(f"Максимальная {max_height} минимальная {min_height} высота картинки")

    def check_max_images(self, good):
        system_settings = core.services.core_service.get_system_settings()
        list_image = models.PartnerGoodImage.objects.filter(product=good)
        if len(list_image) >= system_settings['goods_max_images']:
            raise exceptions.ValidationError(
                detail={'type': 'Превышен лимит изображений для товара'})

    def check_main_image(self, product, is_main):
        images = models.PartnerGoodImage.objects.filter(product=product)
        if not images:
            product.is_active = True
            product.save()
            return True
        elif is_main and images:
            for im in images:
                im.is_main_image = False
                im.save()
        return is_main

    def del_main_image(self, product, image):
        images = models.PartnerGoodImage.objects.filter(product=product).order_by('id')
        if not image in images:
            raise exceptions.ValidationError(
                detail={'type': 'Изображение не принадлежит товару'})
        if len(images) == 1:
            product.is_active = False
            product.save()
        elif image.is_main_image:
            image.is_main_image = False
            image.save()
            for im in images:
                if im != image:
                    im.is_main_image = True
                    im.save()
                    break

    # Управление свойствами товара-------------------------------------------------------------------------------------#
    def create_prop(self, product_properties, id):
        new_props = []
        for prop in product_properties:
            new_props.append(models.PartnerGoodPropertyThrough(
                product_id=id,
                product_property_id=prop['id'],
                value=prop['value']
            )
            )
        models.PartnerGoodPropertyThrough.objects.bulk_create(new_props)
        return new_props

    def update_prop(self, data, id):
        product_properties = data.pop('product_properties')
        props = models.PartnerGoodPropertyThrough.objects.filter(product_id=id)
        for i, item in enumerate(props):
            item.value = product_properties[i]['value']
            item.product_property = models.PartnerGoodProperty.objects.get(id=product_properties[i]['id'])
        models.PartnerGoodPropertyThrough.objects.filter(product_id=id).bulk_update(props,
                                                                                    ['value', 'product_property_id'])
        return data

    # Активация объекта------------------------------------------------------------------------------------------------#
    def active(self, obj, serializer):
        serializer_instance = serializer(obj)
        new_instance = serializer_instance.update(obj, serializer_instance.data)
        new_serializer = serializer(new_instance)
        return new_serializer.data


partner_good_service = PartnerGoodServices()
