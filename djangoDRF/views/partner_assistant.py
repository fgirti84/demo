from django.contrib.auth import get_user_model
from django.db.models import Q
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet

from catalog import models, serialisers, filters
from catalog.permissions import IsPartnerAssistant, IsPartnerOwner
from catalog.services import partner_assistant_service

User = get_user_model()


class PartnerAssistantViewSet(ModelViewSet):
    """
    Помощник.
    """
    http_method_names = ("get", "post", 'put', 'delete')
    permission_classes = [IsAuthenticated, IsPartnerOwner]
    filter_backends = [DjangoFilterBackend]
    filterset_class = filters.PartnerAssistantFilter

    def get_serializer_class(self):
        if self.action == 'accept':
            return serialisers.PartnerAssistantAcceptedSerializer
        if self.request.method == 'POST':
            return serialisers.PartnerAssistantCreateSerializer
        else:
            return serialisers.PartnerAssistantSerializer

    def create(self, request, *args, **kwargs):
        serializer = serialisers.PartnerAssistantCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data, status = partner_assistant_service.create_assistant(
            kwargs.get('partner_id'),
            serializer.validated_data,
            request.user
        )
        return Response(status=status, data=data)

    def get_queryset(self):
        return models.PartnerAssistant.objects.filter(
            partner_id=self.kwargs.get("partner_id")
        ).filter(
            Q(partner__user=self.request.user) | Q(user=self.request.user)
        )

    @action(
        detail=True,
        methods=["post"],
        permission_classes=[IsAuthenticated, IsPartnerAssistant],
        serializer_class=serialisers.PartnerAssistantAcceptedSerializer,
        url_path='accept',
    )
    def accept(self, request, **kwargs):
        """
        Подтверждение назначения помощником
        """
        instance = self.get_object()
        serializer = self.serializer_class(
            instance,
            data=request.data,
            context={"request": request},
            partial=True,
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status=status.HTTP_200_OK, data=serializer.validated_data)


class AdminPartnerAssistantViewSet(ReadOnlyModelViewSet):
    """
    Список помощников партнера.
    """
    serializer_class = serialisers.PartnerAssistantSerializer
    permission_classes = [IsAdminUser]
    filter_backends = [DjangoFilterBackend]
    filterset_class = filters.PartnerAssistantFilter

    def get_queryset(self):
        return models.PartnerAssistant.objects.filter(
            partner_id=self.kwargs.get("partner_id")
        )
