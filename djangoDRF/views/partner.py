from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet

from catalog import filters, models, serialisers
from catalog.services import partner_service


class AdminPartnerViewSet(ReadOnlyModelViewSet):
    serializer_class = serialisers.PartnerSerializer
    permission_classes = [IsAdminUser]
    queryset = models.Partner.objects.all()
    filter_backends = [DjangoFilterBackend]
    filterset_class = filters.PartnerFilter

    @action(
        detail=True,
        methods=["post"],
        serializer_class=None,
        url_path='is_active',
    )
    def is_active(self, request, *args, **kwargs):
        instance = partner_service.partner_is_active_toggle(
            instance=self.get_object(),
        )
        serializer = serialisers.PartnerSerializer(instance=instance)
        return Response(status=status.HTTP_200_OK, data=serializer.data)


class PartnerViewSet(ReadOnlyModelViewSet):
    serializer_class = serialisers.PartnerSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = [DjangoFilterBackend]
    filterset_class = filters.PartnerFilter
    pagination_class = None

    def get_queryset(self):
        return models.Partner.objects.filter(user=self.request.user)
