from rest_framework import exceptions
from django.contrib.auth import get_user_model
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet
from catalog import models, serialisers, filters
from catalog.permissions import IsPartnerOwnerOrAssistant, IsPartnerOrAssistant
from catalog.services import partner_good_service

User = get_user_model()


class AdminPartnerGoodDeliveryMethodViewSet(ModelViewSet):
    """
    Способ доставки товара партнера.
    """

    permission_classes = [IsAdminUser]
    queryset = models.PartnerGoodDeliveryMethod.objects.all()

    def get_serializer_class(self):
        if self.action == 'is_active':
            return serialisers.PartnerGoodDeliveryMethodIsActiveSerializer
        else:
            return serialisers.PartnerGoodDeliveryMethodSerializer

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        partner_good_service.check_link_delivery(instance)
        super().perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(
        detail=True,
        methods=["post"]
    )
    def is_active(self, request, **kwargs):
        """
        Смена активности способа доставки
        """
        new_active = partner_good_service.active(self.get_object(), self.get_serializer_class())
        return Response(status=status.HTTP_200_OK, data=new_active)


class PartnerGoodDeliveryMethodViewSet(ReadOnlyModelViewSet):
    """
    Способ доставки товара партнера.
    """
    serializer_class = serialisers.PartnerGoodDeliveryMethodSerializer
    permission_classes = [IsAuthenticated, IsPartnerOrAssistant]
    queryset = models.PartnerGoodDeliveryMethod.objects.all()


class PartnerGoodImageViewSet(ModelViewSet):
    """
    Картинка товара партнера.
    """
    serializer_class = serialisers.PartnerGoodImageSerializer
    http_method_names = ("post", "delete")
    permission_classes = [IsAuthenticated, IsPartnerOwnerOrAssistant]
    queryset = models.PartnerGoodImage.objects.all()

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        product = models.PartnerGood.objects.filter(id=kwargs['good_id']).first()
        partner_good_service.del_main_image(product, instance)
        super().perform_destroy(instance)

        return Response(status=status.HTTP_204_NO_CONTENT)


class PartnerGoodViewSet(ModelViewSet):
    """
    Товар партнера.
    """
    http_method_names = ("get", "post", "put", "patch")
    permission_classes = [IsAuthenticated, IsPartnerOwnerOrAssistant]
    filter_backends = [DjangoFilterBackend]
    filterset_class = filters.PriceRangePartnerGoodFilter

    def get_serializer_class(self):
        if self.action == 'is_active':
            return serialisers.PartnerGoodIsActiveSerializer
        if self.request.method in ['POST', 'PUT', 'PATCH']:
            return serialisers.PartnerGoodSerializer
        else:
            return serialisers.PartnerGoodGetSerializer

    def get_queryset(self):
        return models.PartnerGood.objects.filter(
            partner_id=self.kwargs.get("partner_id"),
        )

    @action(
        detail=True,
        methods=["post"]
    )
    def is_active(self, request, **kwargs):
        """
        Смена активности товара
        """
        new_active_product = partner_good_service.active(self.get_object(), self.get_serializer_class())
        return Response(status=status.HTTP_200_OK, data=new_active_product)


class AdminPartnerGoodViewSet(ReadOnlyModelViewSet):
    """
    Товар партнера.
    """
    serializer_class = serialisers.PartnerGoodGetSerializer
    permission_classes = [IsAdminUser]

    def get_queryset(self):
        return models.PartnerGood.objects.filter(
            partner_id=self.kwargs.get("partner_id"),
            partner__user=self.request.user,
        )


class PartnerGoodPropertiesViewSet(ModelViewSet):
    """
    Свойства для товара партнера.
    """
    serializer_class = serialisers.PartnerGoodPropertySerializer
    permission_classes = [IsAuthenticated, IsPartnerOrAssistant]
    http_method_names = ("get", "post")
    queryset = models.PartnerGoodProperty.objects.all()


class PartnerGoodCategoriesViewSet(ReadOnlyModelViewSet):
    """
    Категории для товара партнера.
    """
    permission_classes = [IsAuthenticated]
    serializer_class = serialisers.PartnerGoodCategorySerializer

    def get_queryset(self):
        if 'pk' in self.kwargs:
            return models.PartnerGoodCategory.objects.all()
        return models.PartnerGoodCategory.objects.filter(parent_id=None)


class AdminPartnerGoodCategoriesViewSet(ModelViewSet):
    """
    Категории.
    """
    serializer_class = serialisers.PartnerGoodCategorySerializer
    permission_classes = [IsAdminUser]

    def get_queryset(self):
        if 'pk' in self.kwargs:
            return models.PartnerGoodCategory.objects.all()
        return models.PartnerGoodCategory.objects.filter(parent_id=None)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        partner_good_service.check_link_category(instance)
        super().perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)
