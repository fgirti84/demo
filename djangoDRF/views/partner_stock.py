from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet

from catalog import filters, models, serialisers
from catalog.permissions import IsPartnerOwnerOrAssistant


class AdminPartnerStockViewSet(ReadOnlyModelViewSet):
    serializer_class = serialisers.PartnerStockSerializer
    permission_classes = [IsAdminUser]
    queryset = models.PartnerStock.objects.all()
    filter_backends = [DjangoFilterBackend]
    filterset_class = filters.PartnerStockFilter

    def get_queryset(self):
        return models.PartnerStock.objects.filter(
            partner_id=self.kwargs.get('partner_id'),
        )


class PartnerStockViewSet(ModelViewSet):
    http_method_names = ('get', 'post', 'put', 'patch')
    permission_classes = [IsAuthenticated, IsPartnerOwnerOrAssistant]
    filter_backends = [DjangoFilterBackend]
    filterset_class = filters.PartnerStockFilter

    def get_queryset(self):
        return models.PartnerStock.objects.filter(
            partner_id=self.kwargs.get('partner_id'),
            partner__user=self.request.user,
        )

    def get_serializer_class(self):
        if self.request.method in ['POST', 'PUT', 'PATCH']:
            return serialisers.PartnerStockCreateSerializer
        else:
            return serialisers.PartnerStockSerializer
