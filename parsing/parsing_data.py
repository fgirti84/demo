import requests
from bs4 import BeautifulSoup
import os, sys
import django
"Настройка для ручного запуска парсинга"
basedir = os.path.abspath(os.getcwd())
"win"
wordbooks_dir = basedir[:-(len(basedir.split('\\')[-1]) + 1)]
"lin"
# wordbooks_dir = basedir[:-(len(basedir.split('/')[-1]) + 1)]
sys.path.append(wordbooks_dir)

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'search.settings')

django.setup()
from parsing.filling_db import add_data_db


def get_parse_list_offers(data, name_data):
    """
        Возвращает список спарсенных offers
        :data - принимает список(массив) offers в которых содержатся name_data
        :name_data - имя вложенных тегов(child) в которых находится вся информация
    """
    result_list = []
    for el in data:
        for item in el.find_all(name_data):
            element = item.attrs

            for i in item.children:
                if i.name == 'categoryId':
                    element.update({'category_titles': []})
                    list_category = item.find_all(i.name)

                    element.update({
                        'category': i.text
                    })
                    for category in list_category:
                        element['category_titles'].append(category.text)
                if not i.name is None:
                    element.update({
                        i.name: i.text
                    })
            result_list.append(element)
    return result_list


def get_parse_list_categories(data, name_data):
    """
        Возвращает список спарсенных categories
        :data - принимает список(массив) categories в которых содержатся name_data
        :name_data - имя вложенных тегов(child) в которых находится вся информация
    """
    result_list = []
    for el in data:
        for item in el.find_all(name_data):
            category = item.attrs
            category.update({'title': item.text})
            result_list.append(category)
    return result_list


def parsing_data(url):
    """
        Возвращает список спарсенных categories и offers
        И добавляет новые записи в бд при вызове метода add_data_db
    """
    print("Start parsing", url)
    r = requests.get(url)
    host = url.split('/')
    soup = BeautifulSoup(r.content, features="xml")

    categories = soup.find_all('categories')
    offers = soup.find_all('offers')
    list_categories = get_parse_list_categories(categories, 'category')
    list_offers = get_parse_list_offers(offers, 'offer')
    add_data_db(list_offers, list_categories, host[2])
    return list_categories, list_offers
