import os

from interactive.models import ProductInteractive, CategoryInteractive, SynonymsInteractive

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'search.settings')
import django

django.setup()
from arenda.models import ProductArenda, CategoryArenda, SynonymsArenda
from category.models import Product, Category, Synonyms
from fotobudka.models import ProductFotobudka, CategoryFotobudka, SynonymsFotobudka


def add_data_db(data_product, data_category, host):
    """
        Метод по добавлению данных в бд в зависимоти от хоста
    """
    print('Start add data db')
    if host in ():
        add_data(data_product, data_category, Product, Category, Synonyms)
    if host in ():
        add_data(data_product, data_category, ProductArenda, CategoryArenda, SynonymsArenda)
    if host in ():
        add_data(data_product, data_category, ProductFotobudka, CategoryFotobudka, SynonymsFotobudka)
    if host in ():
        add_data(data_product, data_category, ProductInteractive, CategoryInteractive, SynonymsInteractive)


def get_title_categories(data, model):
    """
        Возвращает строку с перечислением title категорий
    """
    titles = []
    result = ''
    if type(data) != list:
        data = [].append(data)

    for item in data:
        if item:
            category = model.objects.filter(id=item).first()
            if category:
                titles.append(category.title)
    if len(titles) != 0:
        for item in titles:
            result += ', '.join(item)
    return result


def check_synonym(item, data):
    """
        Возвращает синоными, если хотя бы 1 синоним в строке не найден в бд
    """
    list_keywords_item = str(item).split(', ')
    for key_word_item in list_keywords_item:
        count = 0
        for el in data:
            list_keywords_el = str(el).split(', ')
            if list_keywords_item == list_keywords_el:
                return None
            if key_word_item not in list_keywords_el:
                count = count + 1
        if count == len(data):
            return item
        return None


def update_data_category(el, item):
    el.title = item['title']
    el.url = item['url']
    el.save()


def update_data(el, item, model):
    el.title = item['model'] if 'model' in item else item['name']
    el.url = item['url']
    el.category = model.objects.filter(id=item['category']).first() if item['category'] else None
    el.picture = item['picture']
    el.price = item['price']
    el.keywords = item['keywords'] if 'keywords' in item else ''
    el.category_titles = get_title_categories(item['category_titles'], Category)
    el.save()


def create_data(item, model, category):
    if model == Product:
        new_product = model.objects.create(
            id=item['id'],
            title=item['model'],
            url=item['url'],
            category=category.objects.filter(id=item['category']).first() if item['category'] else None,
            picture=item['picture'],
            price=item['price'],
            keywords=item['keywords'] if 'keywords' in item else '',
            category_titles=get_title_categories(item['category_titles'], Category),
        )
    else:
        new_product = model.objects.create(
            id=item['id'],
            title=item['name'],
            url=item['url'],
            category=category.objects.filter(id=item['category']).first() if item['category'] else None,
            picture=item['picture'],
            price=item['price'],
            keywords=item['keywords'] if 'keywords' in item else '',
        )
    new_product.save()


def add_data(data_product, data_category, product, category, synonym):
    """
        Добавляет данные в бд
    """
    synonym_db = synonym.objects.all()
    list_new_synonyms = []
    """
        Добавляет данные категории
    """
    if type(data_category) != list:
        data_category = [].append(data_category)
    for item in data_category:
        old_category = category.objects.filter(id=item['id']).first()
        if old_category:
            update_data_category(old_category, item)
        else:
            new_category = category.objects.create(
                id=item['id'],
                title=item['title'],
                url=item['url']
            )
            new_category.save()

    """
        Добавляет данные товара и синонимы
    """
    for item in data_product:
        if 'keywords' in item and item['keywords'] not in list_new_synonyms:
            old_synonym = check_synonym(item['keywords'], synonym_db)
            if old_synonym:
                list_new_synonyms.append(old_synonym)
                new_synonym = synonym.objects.create(text=old_synonym)
                new_synonym.save()
        old_product = product.objects.filter(id=item['id']).first()
        if old_product:
            update_data(old_product, item, category)
        else:
            create_data(item, product, category)


