import os
import schedule
import time

from parsing_data import *
"""Указывается системное время"""
TIME_PARSING = '10:00'
"""Список фидов для парсинга"""
LIST_URL_TO_PARSING = [
   
]


def parsing():
    for url in LIST_URL_TO_PARSING:
        parsing_data(url)


def run_parsing():
    schedule.every().day.at(TIME_PARSING).do(parsing)
    # schedule.every(60).minutes.do(parsing)
    while True:
        schedule.run_pending()
        time.sleep(1)
